console.log("hello philippines");

// [Array]
	// An array in programming is simply a  list of data. Data that are related/ connected with each other.

let studentA = "2020-1923";
let studentB = "2020-1924";
let studentC = "2020-1925";
let studentD = "2020-1926";
let studentE = "2020-1927";

	// now, with an array, we can simply write the code above like this:
	
	let studentNumbers = ["2020-1923", "2020-1924","2020-1925",  "2020-1926", "2020-1927"]

	// Arrays are used to store multiple related values in a single variable.
	// they are declared using sqaure brackets also know as the "Array literals".
	// Commonly used to store numerous amounts of data to manipulate in order to perform a number of task.
	// Array also provide access of functions/method that help achieving specific task
	// a method is another term for functions associated with an object/array and is used to execute statements that are relevant.

	// Property and Method
		// variableName.length - property
		// toLowerCase() - method

	let object = {
					name: "william", 
					age: 17, 
					location: "mayapa"
				}

	console.log(object.name)

	// Array is also object which is another type.

	console.log(typeof studentNumbers);

	/*
		Syntax:
			let/const arrayName = [element0, element1, . . .]
	*/

	// common example of arrays

	const grade = [98.5, 94.3, 89.2, 90.1] // cannot reassigned entire data but can reassign index
	console.log(grade)
	grade[1] = 80;
	/*grade = [];*/

	// another example
	let computerBrands = ["acer", "asus", "lenovo", "dell", "mac", "samsung"];
	console.log(computerBrands);

	// Possible use of an array, but is not recommended
	let mixedArr = [12, 'asus', null, undefined, {}];
	console.log(mixedArr);

	// Create an array with a values from variables
	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "New York";

	// If we use variable as element of our array the value will be passed
	let cities = [city1, city2, city3];
	console.log(cities)

	// [Section] Length Property
		// The .length property allows us to get and set the total number of items/elements in an array

	console.log(grade.length);
	console.log(typeof grade.length);

	let blankArr = [];
	console.log(blankArr.length);

	let array;
	console.log(array);

	// .length property can also be used with strings. Some array methods and properties can also be used.

	// We cant change the letters using this syntax
	let fullName = "john doe";
	console.log(fullName.length);
	fullName.length = fullName.length-1
	console.log(fullName)

	// .length property on strings shows the number of characters in a string. Spaces are counted as characters in strings

	// .length property can also set the number of the items in an array, meaning we can actually delete the las item in the array or shroten the array by simply updating the length property of an array.

	let myTasks = [
			"drink HTML",
			"eat JavaScript",
			"inhale CSS",
			"Bake SASS"
		]
	console.log(myTasks);

	myTasks.length = myTasks.length - 1;
	console.log(myTasks);

/*	function removeElement(array, index){
		let secondArray = array;
		let remove = array.length - index;
		array.length = array.length - remove;
		console.log(array)

		for(let i = index+1; i < secondArray.length-1; i++){
			array += secondArray[i];
		}
		console.log(array);
	}
	removeElement(myTasks, 2)*/


	// to delete a specific item in an array we can employ or use array method

	// if you can shorten the array y setting the length of property, you can also lengthen it by adding a number into length property

	let theBeatles = ["john", "Paul", "ringo", "george"];
	console.log(theBeatles);
	theBeatles[theBeatles.length] = "william"
	console.log(theBeatles);
	theBeatles[theBeatles.length] = "reymart"
	console.log(theBeatles);

// [Section] Reading/Accessing elements of arrays
	// Accessing array elements is one of the more common tast that we do in array
	// this can be done through the use of its index
	// Each element in an array is associated with its index/number

	let lakersLegends = ["kobe", "beverly", "shaq", "lebron", "magic", "westbrook", "kareem"]

	console.log(lakersLegends[1]);
	console.log(lakersLegends[3]);

	// We can also save/store array elements in another variable

	let currentLaker = lakersLegends[2];
	console.log(currentLaker);

	// We can also reassign array value using items indices

	console.log("Array before the reassignment:");
	console.log(lakersLegends)

	lakersLegends[4] = "pau gasol";
	console.log("Array after the reassignment:")
	console.log(lakersLegends)

	// Accessing the last element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value of one allowing us to get the last element

	console.log(lakersLegends[lakersLegends.length-1])

	// Adding elements into array without using array methods

	let newArr = [];
	newArr[newArr.length] = "cloud strife";
	console.log(newArr)
	newArr[newArr.length] = "tifa lockhart";
	console.log(newArr)
	newArr.length = newArr.length - 1
	console.log(newArr)

	let emptyArr = [];
	emptyArr[emptyArr.length] = "pogi"
	console.log(emptyArr)

	// [Section] Looping over an array
		// you can use for loop to iterate over all items in an array

	let numberArr = [5, 12, 30, 46, 40];

	for(let index = 0; index < numberArr.length; index++){
		console.log(numberArr[index])
	}

	for(let index = 0; index < numberArr.length; index++){
		if(numberArr[index] % 5 === 0){
			console.log(numberArr[index], "is divisible by 5")
		}
		else{
			console.log(numberArr[index], "is not divisible by 5")
		}
	}

	// [Section] Multidimensional Arrays
		// Multidimensional arrays are useful for strong complex data structures.
		// a practical application of this is to help visualize/create real world objects
		// though useful in a number, creating compex array structure is not always recommended.

	let chessBoard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
		]

	console.log(chessBoard);
	console.table(chessBoard);
	console.log(chessBoard[0][3]);
	console.log(chessBoard[3][5]);